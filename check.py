import json
from json.decoder import JSONDecodeError
import requests
import os
from pprint import pprint

from envparse import Env

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


def main():
    """
    approve the request
    """
    trigger_payload_file = env('TRIGGER_PAYLOAD', default='')
    with open(trigger_payload_file, 'r') as f:
        payload = json.loads(f.read())
    
    api_token = payload.get('variables', {}).get('API_TOKEN', None)
    project_id = payload.get('project', {}).get('id', None)
    merge_request_iid = payload.get('object_attributes', {}).get('iid', None)
    external_approval_rule_id = payload.get('external_approval_rule', {}).get('id', None)
    sha = payload.get('object_attributes', {}).get('last_commit', {}).get('id', None)
    status = 'passed'  #  pending, passed, failed
    web_url = payload.get('object_attributes', {}).get('url', None)

    headers = {
        'PRIVATE-TOKEN': f'{api_token}',
        'Content-Type': 'application/json',
    }

    failed = False

    print('[x] - Validating code quality did not degrade')
    'curl -X POST  "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/approval_rules"'
    data = {
        'approvals_required': 1, 
        'name': 'Code Quality Approval',
        'usernames': '"smathur"',
    }
    url = f"https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{merge_request_iid}/approval_rules"
    r = requests.post(url, headers=headers, json=data) 
    x = 'x'
    if not r.ok:
        if r.json().get('message', {}).get('name', [])[-1] != 'has already been taken':
            pprint(r.json())
            x = '-'
            failed = True
    print(f'[{x}] - Adding a new MR Approval Rule for Code Quality')


    print('[x] - Validating code coverage did not degrade')
    print('[ ] - Adding a new MR Approval Rule for Code Coverage')

    print('Approving Merge Request for:', web_url)


    if failed:
        exit(1)



    data = {
        'id': f'{project_id}',
        'merge_request_iid': f'{merge_request_iid}',
        'sha': f'{sha}',
        'external_status_check_id': f'{external_approval_rule_id}',
        'status': f'{status}',
    }
    url = f"https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{merge_request_iid}/status_check_responses"

    r = requests.post(url, headers=headers, json=data)

    if r.ok:
        return
    else:
        try:
            pprint(r.json())
        except JSONDecodeError:
            print(r.text)

        print('\n\nHEADERS: ')
        pprint(headers)
        print('\n\nDATA: ')
        pprint(data)
        exit(1)



if __name__ == '__main__':
    main()
